<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>getInfoBizCode</name>
   <tag></tag>
   <elementGuidId>1da6ee78-71cd-4241-b0db-e9e88525cef0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;soapenv:Envelope xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:soapenv=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot; xmlns:ser=&quot;http://servlet.web.webis.ssm.com&quot;>
   &lt;soapenv:Header/>
   &lt;soapenv:Body>
      &lt;ser:getInfoBizCode soapenv:encodingStyle=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>
         &lt;supplyBizCodeReq xsi:type=&quot;urn:SupplyBizCodeReq&quot; xmlns:urn=&quot;urn:SsmWebIs&quot;>
            &lt;agencyId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>?&lt;/agencyId>
            &lt;businessCode xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${businessCode}&lt;/businessCode>
            &lt;tableId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${tableId}&lt;/tableId>
         &lt;/supplyBizCodeReq>
      &lt;/ser:getInfoBizCode>
   &lt;/soapenv:Body>
&lt;/soapenv:Envelope></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>getInfoBizCode</soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.hostname</defaultValue>
      <description></description>
      <id>a8ccf389-26ed-4366-9852-b0dc45ec6270</id>
      <masked>false</masked>
      <name>hostname</name>
   </variables>
   <variables>
      <defaultValue>'16221e'</defaultValue>
      <description></description>
      <id>4ca7b545-838b-400f-8772-79356afe50e2</id>
      <masked>false</masked>
      <name>businessCode</name>
   </variables>
   <variables>
      <defaultValue>'ROBINFO'</defaultValue>
      <description></description>
      <id>5f671674-3ea7-4f10-a48a-dc188592aff8</id>
      <masked>false</masked>
      <name>tableId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress>http://${hostname}/SSMWEBIS_SEC/services/InfoService?wsdl</wsdlAddress>
</WebServiceRequestEntity>
