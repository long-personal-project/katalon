<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>getInfoChargeList</name>
   <tag></tag>
   <elementGuidId>c34d9d32-24c5-4bc7-84f9-bb3068433b51</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;soapenv:Envelope xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:soapenv=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot; xmlns:ser=&quot;http://servlet.web.webis.ssm.com&quot;>
   &lt;soapenv:Header/>
   &lt;soapenv:Body>
      &lt;ser:getInfoChargeList soapenv:encodingStyle=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>
         &lt;supplyChargesReq xsi:type=&quot;urn:SupplyChargesReq&quot; xmlns:urn=&quot;urn:SsmWebIs&quot;>
            &lt;agencyId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/agencyId>
            &lt;coNo xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${coNo}&lt;/coNo>
            &lt;gstAmount xsi:type=&quot;soapenc:decimal&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/gstAmount>
            &lt;infoAmount xsi:type=&quot;soapenc:decimal&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/infoAmount>
            &lt;invoiceNo xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/invoiceNo>
            &lt;ipaddress xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/ipaddress>
            &lt;remark xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/remark>
            &lt;reqRefNo xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${reqRefNo}&lt;/reqRefNo>
            &lt;tableId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/tableId>
            &lt;type xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${type}&lt;/type>
         &lt;/supplyChargesReq>
      &lt;/ser:getInfoChargeList>
   &lt;/soapenv:Body>
&lt;/soapenv:Envelope></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>getInfoChargeList</soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.hostname</defaultValue>
      <description></description>
      <id>fcab2c22-5226-471f-bd36-f005d4b2d15d</id>
      <masked>false</masked>
      <name>hostname</name>
   </variables>
   <variables>
      <defaultValue>'582342'</defaultValue>
      <description></description>
      <id>9a392486-9268-4164-a414-d961f5dc0d0f</id>
      <masked>false</masked>
      <name>coNo</name>
   </variables>
   <variables>
      <defaultValue>'abc123'</defaultValue>
      <description></description>
      <id>694b7f56-5123-4f6a-8a87-a93d9d1671b7</id>
      <masked>false</masked>
      <name>reqRefNo</name>
   </variables>
   <variables>
      <defaultValue>'INFOROCCHRGE'</defaultValue>
      <description></description>
      <id>5b520489-11df-41fa-b5fb-f01c7f3dc04d</id>
      <masked>false</masked>
      <name>type</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress>http://${hostname}/SSMWEBIS_SEC/services/InfoService?wsdl</wsdlAddress>
</WebServiceRequestEntity>
