<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>getInfoBranchListing</name>
   <tag></tag>
   <elementGuidId>3483a8b1-c144-45fe-9eae-57b64b459b1c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;soapenv:Envelope xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:soapenv=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot; xmlns:ser=&quot;http://servlet.web.webis.ssm.com&quot;>
   &lt;soapenv:Header/>
   &lt;soapenv:Body>
      &lt;ser:getInfoBranchListing soapenv:encodingStyle=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>
         &lt;supplyBranchReq xsi:type=&quot;urn:SupplyBranchReq&quot; xmlns:urn=&quot;urn:SsmWebIs&quot;>
            &lt;addressId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>0&lt;/addressId>
            &lt;agencyId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/agencyId>
            &lt;brNo xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${brNo}&lt;/brNo>
            &lt;gstAmount xsi:type=&quot;soapenc:decimal&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/gstAmount>
            &lt;infoAmount xsi:type=&quot;soapenc:decimal&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/infoAmount>
            &lt;invoiceNo xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/invoiceNo>
            &lt;ipaddress xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/ipaddress>
            &lt;remark xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/remark>
            &lt;reqRefNo xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>&lt;/reqRefNo>
            &lt;type xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${type}&lt;/type>
         &lt;/supplyBranchReq>
      &lt;/ser:getInfoBranchListing>
   &lt;/soapenv:Body>
&lt;/soapenv:Envelope></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>getInfoBranchListing</soapServiceFunction>
   <variables>
      <defaultValue>'002316920'</defaultValue>
      <description></description>
      <id>afaacd59-5f4a-4ed8-a8d8-6fd25654f8c7</id>
      <masked>false</masked>
      <name>brNo</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.hostname</defaultValue>
      <description></description>
      <id>7f88b5e7-bf50-4868-b601-750fe06fac96</id>
      <masked>false</masked>
      <name>hostname</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>b2eb7637-bed2-4903-bd95-a212a62c7d3a</id>
      <masked>false</masked>
      <name>type</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress>http://${hostname}/SSMWEBIS_SEC/services/InfoService?wsdl</wsdlAddress>
</WebServiceRequestEntity>
