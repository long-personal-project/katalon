<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>isOwnerValidWithIc</name>
   <tag></tag>
   <elementGuidId>46aa4dd0-bdc4-41c2-96c0-2f6315c60af4</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;soapenv:Envelope xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:soapenv=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot; xmlns:ser=&quot;http://servlet.web.webis.ssm.com&quot;>
   &lt;soapenv:Header/>
   &lt;soapenv:Body>
      &lt;ser:isOwnerValidWithIc soapenv:encodingStyle=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>
         &lt;businessFormAOwnerValidReq xsi:type=&quot;urn:BusinessFormAOwnerValidReq&quot; xmlns:urn=&quot;urn:SsmWebIs&quot;>
            &lt;agencyBranchCode xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>?&lt;/agencyBranchCode>
            &lt;agencyId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${agencyId}&lt;/agencyId>
            &lt;reqRefNo xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>?&lt;/reqRefNo>
            &lt;icNo xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${IcNo}&lt;/icNo>
            &lt;name xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${name}&lt;/name>
         &lt;/businessFormAOwnerValidReq>
      &lt;/ser:isOwnerValidWithIc>
   &lt;/soapenv:Body>
&lt;/soapenv:Envelope></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>findBusinessByICNo</soapServiceFunction>
   <variables>
      <defaultValue>'NUR ILYANA BINTI SAMSUDIN'</defaultValue>
      <description></description>
      <id>dc81c591-7335-4d99-8008-ad047e9aeef1</id>
      <masked>false</masked>
      <name>name</name>
   </variables>
   <variables>
      <defaultValue>'891123145632'</defaultValue>
      <description></description>
      <id>bdce9ac1-ee64-4a19-bf3f-e85de2724ec7</id>
      <masked>false</masked>
      <name>IcNo</name>
   </variables>
   <variables>
      <defaultValue>'MB'</defaultValue>
      <description></description>
      <id>beeac9f0-4d9c-4184-86e6-997c7f2704ab</id>
      <masked>false</masked>
      <name>agencyId</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.hostname</defaultValue>
      <description></description>
      <id>f3425b6c-1a47-4836-8fdd-88388f44d836</id>
      <masked>false</masked>
      <name>hostname</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress>http://${hostname}/SSMWEBIS_SEC/services/BusinessService?wsdl</wsdlAddress>
</WebServiceRequestEntity>
