<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>getInfoParam</name>
   <tag></tag>
   <elementGuidId>5b61a780-0131-4c56-bad2-59860a981a69</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;soapenv:Envelope xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns:soapenv=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot; xmlns:ser=&quot;http://servlet.web.webis.ssm.com&quot;>
   &lt;soapenv:Header/>
   &lt;soapenv:Body>
      &lt;ser:getInfoParam soapenv:encodingStyle=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>
         &lt;supplyParamReq xsi:type=&quot;urn:SupplyParamReq&quot; xmlns:urn=&quot;urn:SsmWebIs&quot;>
            &lt;agencyId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>?&lt;/agencyId>
            &lt;code xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${code}&lt;/code>
            &lt;paramType xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${paramType}&lt;/paramType>
            &lt;tableId xsi:type=&quot;soapenc:string&quot; xmlns:soapenc=&quot;http://schemas.xmlsoap.org/soap/encoding/&quot;>${tableId}&lt;/tableId>
         &lt;/supplyParamReq>
      &lt;/ser:getInfoParam>
   &lt;/soapenv:Body>
&lt;/soapenv:Envelope>
</soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>getInfoParam</soapServiceFunction>
   <variables>
      <defaultValue>'RUM'</defaultValue>
      <description></description>
      <id>338a7a7e-8e92-4214-8a32-73a09ab30739</id>
      <masked>false</masked>
      <name>code</name>
   </variables>
   <variables>
      <defaultValue>'ROBINFO'</defaultValue>
      <description></description>
      <id>21160b07-bb34-4f1d-94e7-34e3693ae07d</id>
      <masked>false</masked>
      <name>tableId</name>
   </variables>
   <variables>
      <defaultValue>'Country'</defaultValue>
      <description></description>
      <id>30abd1ba-5cef-4b8a-9838-14adaa4920f2</id>
      <masked>false</masked>
      <name>paramType</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress>http://${lsbHost}/EZBIZ/services/InfoService?wsdl</wsdlAddress>
</WebServiceRequestEntity>
