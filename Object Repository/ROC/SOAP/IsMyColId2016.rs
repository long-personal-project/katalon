<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>IsMyColId2016</name>
   <tag></tag>
   <elementGuidId>8322e340-dfec-467d-8e94-d3e78b9793ff</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;soapenv:Envelope xmlns:soapenv=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot; xmlns:roc=&quot;http://roc.ssm.org/&quot;>
   &lt;soapenv:Header/>
   &lt;soapenv:Body>
      &lt;roc:IsMyColId2016>
         &lt;!--Optional:-->
         &lt;roc:request>
            &lt;!--Optional:-->
            &lt;roc:EventName>?&lt;/roc:EventName>
            &lt;!--Optional:-->
            &lt;roc:Header>
               &lt;!--Optional:-->
               &lt;roc:EventName>?&lt;/roc:EventName>
               &lt;!--Optional:-->
               &lt;roc:TransactionReferenceId>?&lt;/roc:TransactionReferenceId>
            &lt;/roc:Header>
            &lt;!--Optional:-->
            &lt;roc:Body>
               &lt;!--Optional:-->
               &lt;roc:AgencyId>${AgencyId}&lt;/roc:AgencyId>
               &lt;!--Optional:-->
               &lt;roc:CheckDigit>ghjghjj&lt;/roc:CheckDigit>
               &lt;!--Optional:-->
               &lt;roc:CompanyRegistrationNumber>?&lt;/roc:CompanyRegistrationNumber>
            &lt;/roc:Body>
         &lt;/roc:request>
      &lt;/roc:IsMyColId2016>
   &lt;/soapenv:Body>
&lt;/soapenv:Envelope></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP</soapRequestMethod>
   <soapServiceFunction>IsMyColId2016</soapServiceFunction>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>4c464be3-728e-4872-b3b0-72857dd089a6</id>
      <masked>false</masked>
      <name>AgencyId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyElementText(response, &quot;IsMyColId2016Response.IsMyColId2016Result.Body.MyCoId2016&quot;, &quot;false&quot;)
WS.verifyElementText(response, 'IsMyColId2016Response.IsMyColId2016Result.Header.ErrorCode', '00')</verificationScript>
   <wsdlAddress>http://ssm-roc-app01.southeastasia.cloudapp.azure.com/ROCIntegrationWS/WebService/ROC/ROCResponse.asmx?wsdl</wsdlAddress>
</WebServiceRequestEntity>
