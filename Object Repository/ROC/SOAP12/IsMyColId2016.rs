<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>IsMyColId2016</name>
   <tag></tag>
   <elementGuidId>9878da92-32c8-469b-b435-4c4c749f4a62</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <restRequestMethod></restRequestMethod>
   <restUrl></restUrl>
   <serviceType>SOAP</serviceType>
   <soapBody>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?>&lt;SOAP-ENV:Envelope xmlns:SOAP-ENV=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot; xmlns:tns=&quot;http://roc.ssm.org/&quot;>
  &lt;SOAP-ENV:Header/>
  &lt;SOAP-ENV:Body>
    &lt;tns:IsMyColId2016>
      &lt;tns:request>?&lt;/tns:request>
    &lt;/tns:IsMyColId2016>
  &lt;/SOAP-ENV:Body>
&lt;/SOAP-ENV:Envelope>
</soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod>SOAP12</soapRequestMethod>
   <soapServiceFunction>IsMyColId2016</soapServiceFunction>
   <wsdlAddress>http://ssm-roc-app01.southeastasia.cloudapp.azure.com/ROCIntegrationWS/WebService/ROC/ROCResponse.asmx?wsdl</wsdlAddress>
</WebServiceRequestEntity>
