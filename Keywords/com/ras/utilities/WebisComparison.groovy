package com.ras.utilities
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.exception.StepErrorException
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil as KU
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import internal.GlobalVariable


public class WebisComparison {

	static void verifySameResponseValues(ResponseObject responseA, ResponseObject responseB, ArrayList<String> paths, nullable=false) {
		def parsedA = new XmlSlurper().parseText(responseA.getResponseText())
		def parsedB = new XmlSlurper().parseText(responseB.getResponseText())
		for (String path in paths) {
			def elA = parsedA.Body
			def elB = parsedB.Body
			for (item in path.split("\\.")) {
				elA = elA[item]
				elB = elB[item]
			}
			if(elA.text() != elB.text()) {
				KU.markFailedAndStop("Response path mismatch for path "+ path + "\nExpected " + elA.text() + " but got " + elB.text())
			} else if(!nullable && elB.text() == null) {
				KU.markFailedAndStop("Response path cannot be null for path "+ path)
			} else {
				KU.logInfo("Values match for path " + path + ": " + elB.text())
			}
		}
	}

	static void verifyResponseValues(ResponseObject response, ArrayList<ArrayList<Object>> paths ) {
		def body = new XmlSlurper().parseText(response.getResponseText()).Body
		paths.each {
			def (path, value) = it
			ArrayList<String> pathItems = path.split("\\.")  // 'getItemResponse.getItemReturn.packageType'
			def targetElement = pathItems.inject(body) {
			    element, currentPath -> element[currentPath]
			}
			if(targetElement.text() != value) {
				KU.markFailedAndStop("Response path mismatch for path "+ path + "\nExpected " + value + " but got " + targetElement.text())
			}
		}
	}

	static void verifyResponseElementsExist(ResponseObject response, ArrayList<String> paths) {
		def body = new XmlSlurper().parseText(response.getResponseText()).Body
		paths.each {
			def path = it
			ArrayList<String> pathItems = path.split("\\.")
			if(body.size() <= 0){
				KU.markFailedAndStop("Missing element in the response: " + path)
			}
			def targetElement = pathItems.inject(body) { element, currentPath ->
				if(element[currentPath].size() <= 0) {
					KU.markFailedAndStop("Missing element in the response: " + path)
				}
				element[currentPath]
			}
		}
	}
	
	static void verifyResponseElementsValueExistence(ResponseObject response, ArrayList<String> paths, Boolean isEmpty) {
		def body = new XmlSlurper().parseText(response.getResponseText()).Body
		def result
		paths.each {
			def path = it
			ArrayList<String> pathItems = path.split("\\.")
			if (body.size() <= 0) {
				KU.markFailedAndStop("Missing element in the response: " + path)
			}
			def targetElement = pathItems.inject(body) { element, currentPath ->
				println(path)
				if (element[currentPath].size() <= 0) {
					KU.markFailedAndStop("Missing element in the response: " + path)
				}
				element[currentPath]
				result = element[currentPath]
			}
			if (isEmpty) {
				// if the passed value is true and the field is not empty, raise error
				if (result != '') {
					KU.markFailedAndStop(path + ' is not empty with value ' + result)
				}
			} else {
				// if the passed value is false and the field is empty, raise error
				if (result == '') {
					KU.markFailedAndStop(path + ' is empty')
				}
			}
		}
	}

	static verifyResponseElementsValueNotEmpty(ResponseObject response, ArrayList<String> paths) {
		return WebisComparison.verifyResponseElementsValueExistence(response, paths, false)
	}
	
	static verifyResponseElementsValueEmpty(ResponseObject response, ArrayList<String> paths) {
		return WebisComparison.verifyResponseElementsValueExistence(response, paths, true)
	}

	static def verifySameError(ResponseObject responseA, ResponseObject responseB, ArrayList<String> basePath) {
		return WebisComparison.verifySameError(responseA, responseB, basePath.join("."))
	}

	static def verifySameError(ResponseObject responseA, ResponseObject responseB, String basePath) {
		return WebisComparison.verifySameResponseValues(responseA, responseB, ["errorMsg", "successCode"].collect { basePath + "." + it })
	}

	static ResponseObject buildResponse(String testObject, String url, Map variables=[:]) {
		variables.url = url
		return WS.sendRequest(findTestObject(testObject, variables))
	}

	static ResponseObject buildWebisResponse(String testObject, Map variables=[:]) {
		return WebisComparison.buildResponse(testObject, GlobalVariable.webisHost, variables)
	}

	static ResponseObject buildLSBResponse(String testObject, Map variables=[:]) {
		return WebisComparison.buildResponse(testObject, GlobalVariable.lsbHost, variables)
	}

	static ArrayList<ResponseObject> buildWebisAndLSBResponse(String testObject, Map variables=[:]) {
		return [WebisComparison.buildWebisResponse(testObject, variables), WebisComparison.buildLSBResponse(testObject, variables)]
	}
	
	
	static void verifyDateFormat(String date, regex=/^\d{4}\-\d{2}\-\d{2}[T]\d{2}\:\d{2}\:\d{2}/) {
		boolean match = date =~ regex
		if (!match) {
			KU.markFailedAndStop("Date time given doesn't match regex")
		}
	}
	
	static void verifyDateFormatFromPaths(ResponseObject resp, ArrayList<String> paths, regex=/^\d{4}\-\d{2}\-\d{2}[T]\d{2}\:\d{2}\:\d{2}/) {
		def response = new XmlSlurper().parseText(resp.getResponseText()).Body
		paths.each {
			def path = it
			ArrayList<String> pathItems = path.split("\\.")
			def elem = response[pathItems[0]]
			for (int i = 1; i < pathItems.size(); i++) {
				if (pathItems[i] =~ /\[\d+\]$/) {
					// grab integer from string
					int k = pathItems[i].findAll(/\[\d+\]/)[0].findAll(/\d+/)[0].toInteger()

					// remove integer from string
					String tempPath = pathItems[i] - ~/\[\d+\]$/

					elem = elem[tempPath][k]
				} else {
					elem = elem[pathItems[i]]
				}
			}
			def targetElement = elem
			WebisComparison.verifyDateFormat(targetElement.toString(), regex=regex)
		}
	}
}
