import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GV
import org.openqa.selenium.Keys as Keys
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GV.robWeb)

WebUI.setText(findTestObject('Object Repository/web/ROB/Page_Access Manager/input_Please enter your credentials_Ecom_User_ID'), 
    GV.regularUsername)

WebUI.setEncryptedText(findTestObject('Object Repository/web/ROB/Page_Access Manager/input_Please enter your credentials_Ecom_Password'), 
    GV.regularPassword)

WebUI.click(findTestObject('Object Repository/web/ROB/Page_Access Manager/span_Sign in'))

WebUI.waitForJQueryLoad(10)

WebUI.verifyMatch(WebUI.getUrl(), GV.robWeb + '/Application/Index', false)

WebUI.closeBrowser()

