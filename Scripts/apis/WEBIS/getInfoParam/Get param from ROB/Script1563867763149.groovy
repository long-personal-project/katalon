import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.ras.utilities.WebisComparison

import internal.GlobalVariable

def response = WS.sendRequest(findTestObject('WEBIS/SOAP/getInfoParam', [
	tableId: 'ROBINFO', paramType: 'IDCardType', code: 'S',
	lsbHost: GlobalVariable.lsbHost
]))

WebisComparison.verifyResponseValues(response, [
	['getInfoParamResponse.getInfoParamReturn.description', 'POLICE'],
	['getInfoParamResponse.getInfoParamReturn.paramType', 'IDCardType'],
])
