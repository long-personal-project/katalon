import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.ras.utilities.WebisComparison

import internal.GlobalVariable

def tableId = 'somethingrandom'

def (oldResponse, newResponse) = WebisComparison.buildWebisAndLSBResponse('WEBIS/SOAP/getInfoParam', [
	tableId : tableId,
])

WebisComparison.verifySameError(oldResponse, newResponse, ['getInfoParamResponse', 'getInfoParamReturn'])


