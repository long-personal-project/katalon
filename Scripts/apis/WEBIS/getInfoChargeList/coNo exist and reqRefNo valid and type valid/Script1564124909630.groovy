import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.ras.utilities.WebisComparison as WebisComparison
import com.kms.katalon.core.logging.KeywordLogger

def response = WS.sendRequest(findTestObject(
	'WEBIS/SOAP/getInfoChargeList',
	[
		coNo: '582342',
		reqRefNo: 'lorem ipsum',
		type: 'INFOROCCHRGE',
		hostname: GlobalVariable.hostname
	]
))

WebisComparison.verifyResponseValues(
	response,
	[
		['getInfoChargeListResponse.getInfoChargeListReturn.errorMsg', ''],
		['getInfoChargeListResponse.getInfoChargeListReturn.successCode', '00'],
	]
)

// verify date format is correct
def date = new XmlSlurper().parseText(response.getResponseText()).Body.getInfoChargeListResponse.getInfoChargeListReturn.SSMRegistrationChargesInfos.SSMRegistrationChargesInfos[0].chargeCreateDate
WebisComparison.verifyDateFormat(date.toString())

// verify charge amount
def chargeAmount = new XmlSlurper().parseText(response.getResponseText()).Body.getInfoChargeListResponse.getInfoChargeListReturn.SSMRegistrationChargesInfos.SSMRegistrationChargesInfos[0].chargeAmount
WS.verifyEqual(chargeAmount, 1100000.00)

// verify chargeeAddr1
def chargeeAddr1 = new XmlSlurper().parseText(response.getResponseText()).Body.getInfoChargeListResponse.getInfoChargeListReturn.SSMRegistrationChargesInfos.SSMRegistrationChargesInfos[0].chargeeAddr1
WS.verifyEqual(chargeeAddr1, "LOT E002,GROUND FLOOR,BLOCK 2200")

WebisComparison.verifyDateFormatFromPaths(
	response, 
	["getInfoChargeListResponse.getInfoChargeListReturn.SSMRegistrationChargesInfos.SSMRegistrationChargesInfos[0].chargeCreateDate"]
)

//<?xml version="1.0" encoding="UTF-8"?>
//<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
//  <soapenv:Body>
//    <ns1:getInfoChargeListResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://servlet.web.webis.ssm.com">
//      <getInfoChargeListReturn xsi:type="ns2:SSMRegistrationChargesListInfo" xmlns:ns2="urn:SsmWebIs">
//        <SSMRegistrationChargesInfos soapenc:arrayType="ns2:SSMRegistrationChargesInfo[29]" xsi:type="soapenc:Array" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">1100000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2005-08-10T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">001
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">LOT E002,GROUND FLOOR,BLOCK 2200
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">ENT. 3 BUILDING,PERSIARAN APEC
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">63000 CYBERJAYA,SELANGOR
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2005-09-20T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">PLACEMENT 100% MARGINAL DEPOSIT UNDER GIA AND WHEN THE FACILITIES ARE UTILISED
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">A LETTER OF SET-OFF FOR FIXED DEPOSIT AND/OR NEGOTIABLE OF DEPOSIT
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">2000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2005-12-11T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">002
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">3RD FLOOR, MENARA MAYBANK
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">100 JALAN TUN PERAK
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50050 K.L.
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <propertiesAffected xsi:type="soapenc:string">FIXED DEPOSIT CERTIFICATES NOW ISSUED OR FROM TIME TO TIME BY THE CHARGEE UNDER ACCOUNT OR ACCOUNTS OR ANY OTHER ACCOUNT WITH THE CHARGEE.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2014-02-13T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT OF FIXED DEPOSIT RECEIPT AND LETTER OF SET-OFF
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">5000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2007-10-04T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">003
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">D
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">37TH FLOOR, MENARA MAYBANK,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">NO. 100 JALAN TUN PERAK
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50050,KUALA LUMPUR,WILAYAH PERSEKUTUAN
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2007-10-10T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">PRESENT OR FUTURE ASSETS OF THE COMPANY.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2013-01-01T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">NEGATIVE PLEDGE
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">3000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2007-10-04T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">004
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">37TH FLOOR, MENARA MAYBANK,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">NO. 100 JALAN TUN PERAK
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50050,KUALA LUMPUR,WILAYAH PERSEKUTUAN
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2007-10-10T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">THE CHARGEE CHARGING MONEYS FROM TIME TO TIME STANDING TO THE CREDIT OF THE CHARGOR'S DESIGNATED DEPOSIT ACCOUNT WITH THE CHARGEE OR SUCH OTHER DEPOSIT ACCOUNTS.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2014-02-13T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT OF FIXED DEPOSIT RECEIPT AND LETTER OF SET-OFF - RM3,000,000.00
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">2000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2007-10-04T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">005
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">37TH FLOOR, MENARA MAYBANK,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">NO. 100 JALAN TUN PERAK
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50050,KUALA LUMPUR,WILAYAH PERSEKUTUAN
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2007-10-10T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">THE CHARGEE CHARGING MONEYS FROM TIME TO TIME STANDING TO THE CREDIT OF THE CHARGOR'S DESIGNATED DEPOSIT ACCOUNT WITH THE CHARGEE OR SUCH OTHER ACCOUNT.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2014-02-13T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT OF FIXED DEPOSIT CERTIFICATE AND/OR MUDHARABAH INVESTMENT ACCOUNT CERTIFICATE AND LETTER OF SET-OFF - RM2,000,000.00
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">1300000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2007-11-05T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">006
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">D
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">14TH FLOOR, MENARA MAYBANK,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">100 JALAN TUN PERAK, 
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50050 KUALA LUMPUR
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">MAYBANK ISLAMIC BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">MAYBANK ISLAMIC BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2007-11-12T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">PRESENT OR FUTURE ASSETS OF THE COMPANY.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2013-01-09T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">NEGATIVE PLEDGE
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">0.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2007-10-22T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">O
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">007
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">1 &amp; 3 JALAN M/J3, TAMAN MAJLIS JAYA
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">JALAN SUNGAI CHUA
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">43000 KAJANG, SELANGOR
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">PUBLIC BANK BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">PUBLIC BANK BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2007-11-14T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">ALL SUMS OF MONEYS IN WHATSOEVER CURRENCY WHICH ARE FROM TIME TO TIME DEPOSITED BY THE COMPANY WITH THE CHARGEE AT ANY BRANCH OF THE CHARGEE AND ALL INTEREST ACCRUED AND ACCRUING FROM TIME TO TIME IN RESPECT OF THE DEPOSIT.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">LETTER OF SET-OFF
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">1500000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2008-05-08T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">008
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">TINGKAT 14, MENARA MAYBANK,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">NO. 100 JALAN TUN PERAK
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50050,KUALA LUMPUR,WILAYAH PERSEKUTUAN
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2008-05-13T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT OF FIXED DEPOSIT RECEIPT DATED 09052008 EXECUTED BY PC3 TECHNOLOGY SDN. BHD. IN FAVOUR OF THE BANK CHARGING MONEYS FROM TIME TO TIME STANDING TO THE CREDIT OF THE COMPANY DESIGNATED DEPOSIT ACCOUNT WITH THE BANK.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2014-02-13T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FACILITY AGREEMENT, MEMORANDUM OF DEPOSIT OF FIXED DEPOSIT RECEIPT, LETTER OF SET-OFF
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">4512500.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2008-06-11T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">009
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">TINGKAT 1 BLOK PODIUM, MENARA BUMIPUTRA
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">21 JALAN MELAKA
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50100 KUALA LUMPUR
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2008-06-15T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">MONEYS FROM TIME TO TIME STANDING TO THE CREDIT OF THE CHARGOR'S DESIGNATED DEPOSIT ACCOUNT WITH THE CHARGEE OR SUCH OTHER DEPOSIT ACCOUNTS  THE CHARGEE INCLUDING SUCH ADDITIONAL MONEYS OR DEPOSITS BY THE CHARGOR FROM TIME TO TIME IN THE DEPOSIT ACCOUNT OR SUCH OTHER DEPOSIT ACCOUNTS.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2015-07-12T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">LETTER OF AUTHORISATION TO UPLIFT THE GENERAL INVESTMENT ACCOUNTS (SINKING FUND)
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">4070000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2008-06-11T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">010
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">TINGKAT 1, BLOK PODIUM 
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">MENARA BUMIPUTRA, 21 JLN. MELAKA
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50100 KUALA LUMPUR
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2008-06-15T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">GENERAL INVESTMENT ACCOUNT CERTIFICATE NOW ISSUED OR FROM TIME TO TIME BY THE CHARGEE UNDER ACCOUNT OR ACCOUNTS OR ANY OTHER ACCOUNT WITH THE CHARGEE.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2015-07-12T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">LETTER OF AUTHORISATION TO UPLIFT THE GENERAL INVESTMENT ACCOUNTS (MARGINAL DEPOSIT)
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">3000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2008-12-22T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">011
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">TINGKAT 14, MENARA MAYBANK,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">NO. 100 JALAN TUN PERAK
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50050,KUALA LUMPUR,WILAYAH PERSEKUTUAN
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">MALAYAN BANKING BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2008-12-25T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">(I) AND (II) MOD OF FIXED DEPOSIT AND LETTER OF SET-OFF OF 30% MARGINAL DEPOSIT - MEMORANDUM OF DEPOSIT RESEIPT AND LETTER OF SET-OFF BOTH DATED 23122008 DULY EXECUTED BY PC3 TECHNOLOGY SDN BHD IN FAVOUR OF THE BANK CHARGING MONEYS FROM TIEM TO TIME STANDING TO THE CREDIT OF THE COMPANY DESIGNATED DEPOSIT ACCOUNT WITH THE BANK; (III) AND (IV) MOD OF FIXED DEPOSIT RECEIPT AND LETTER OF SET-OFF OF 50% MARGINAL DEPOSIT - MEMORANDUM OF DEPOSIT OF FIXED DEPOSIT RECEIPT AND LETTER OF SET-OFF BOTH DATED 23122008 DULY EXECUTED BY PC3 TECHNOLOGY SDN BHD IN FAVOUR OF THE BANK CHARGING MONEYS FROM TIME TO TIME STANDING TO THE CREDIT OF THE COMPANY DESIGNATED DEPOSIT ACCOUNT WITH THE BANK.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2014-02-13T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FACILITIES AGREEMENT, MOD OF FIXED DEPOSIT RECEIPT OF 30% MARGINAL DEPOSIT, LETTER OF SET-OFF 30% MARGINAL DEPOSIT, MOD OF FIXED DEPOSIT OF 50% MARGINAL DEPOSIT, LETTER OF SET-OFF OF 50% MARGINAL DEPOSIT
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">2000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2010-07-19T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">012
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">NIO.3, JALAN 45/26,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">TAMAN SRI RAMPAI,
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">HONG LEONG BANK  BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">HONG LEONG BANK  BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2010-07-22T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">FIXED DEPOSIT CERTIFICATES NOW ISSUED OR FROM TIME TO TIME BY THE CHARGEE UNDER ACCOUNT OR ACCOUNTS OF ANY OTHER ACCOUNT WITH THE CHARGEE.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2013-04-07T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">LETTER OF SET-OFF (FIRST PARTY)
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">250000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2010-07-19T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">013
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">LOT 7-2, BLOCK 7
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">ARAB-MALAYSIAN BUSINESS CENTRE
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">JLN PASAR, 70000 SEREMBAN, NS
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">AMBANK (M) BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">AMBANK (M) BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2010-07-27T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">GENERAL AGREEMENT FOR RM1,250,000.00 TOGETHER WITH INTEREST, COMMISSION AND ALL OTHER BANK CHARGES
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">GENERAL AGREEMENT; LETTER OF GUARANTEE; MEMORANDUM OF DEPOSIT AND LETTER OF SET-OFF
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">250000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2010-07-19T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">014
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">LOT 7-2, BLOCK 7
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">ARAB-MALAYSIAN BUSINESS CENTRE
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">JLN PASAR, 70450 SEREMBAN, NS
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">AMBANK (M) BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">AMBANK (M) BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2010-08-17T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">FACILITY AGREEMENT FOR RM1,250,000.00 TOGETHER WITH INTEREST, COMMISSION AND ALL OTHER BANK CHARGES.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FACILITY AGREEMENT, LETTER OF GUARANTEE, MEMORANDUM OF DEPOSIT AND LETTER OF SET-OFF
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">5000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2010-12-14T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">015
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">11TH FLOOR, WISMA BANK ISLAM
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">JALAN DUNGUN, BUKIT DAMANSARA
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50490 KUALA LUMPUR
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK ISLAM MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK ISLAM MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2010-12-14T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">TO REPLACE THE EXISTING PLEDGE THIRD PARTY GIA OF RM1MILLION UNDER THE NAME OF CHIN BOON LONG TO PLEDGE OF FIRST PARTY GIA UNDER THE NAME OF PC3 TECHNOLOGY SDN BHD OF RM1MILLION. PROFIT ACCUMULATED SHALL BE CAPITALIZED AND FORMED AS PART OF THE SECURITY THEREOF.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2016-12-10T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">4000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2010-11-07T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">016
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">,,
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">RHB ISLAMIC BANK BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">RHB ISLAMIC BANK BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2011-01-05T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">PLEDGE UNDER A MUDHARABAH GENERAL INVESTMENT ACCOUNT FOR THE AMOUNT OF RM1500000.00 TOGETHER WITH PROFIT EARNED THEREON TO BE CAPITALIZED AND INCLUDE ANY OTHER ACCOUNTS AND/OR RECEIPT ISSUED FROM TIME TO TIME IN REPLACEMENT OR IN SUBSTITUTION THEREAFTER OR ANY OTHER MONEYS DEPOSITED WITH THE CHARGEE IN ADDITION THERETO
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FIRST PARTY LETTER OF SET-OFF FOR MUDHARABAH GENERAL INVESTMENT ACCOUNT(S)  AND MEMORANDUM OF DEPOSIT FOR MUDHARABAH GENERAL INVESTMENT ACCOUNT.
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">0.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2010-01-13T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">O
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">017
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">,,
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">RHB ISLAMIC BANK BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">RHB ISLAMIC BANK BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string" xsi:nil="true"/>
//            <form40Date xsi:type="xsd:dateTime">2011-03-01T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">25% CASH MARGIN UPON EVERY ISSUANCE OF LETTER OF CREDIT-I (LC-I)/ TRUST RECEPT-I (TR-I)/ ACCEPTED BILL-I. RELEASE OF THE CASH MARGIN SHALL ONLY BE ALLOWED UPON FULL SETTLEMENT
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FIRST PARTY LETTER OF SET-OFF GENERAL INVESTMENT ACCOUNT (S) (GIA) AND MEMORANDUM OF DEPOSIT OF MUDHARABAH GENERAL INVESTMENT ACCOUNT RECEIPT
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">0.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2010-07-28T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">O
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">018
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">,
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">,,
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">RHB ISLAMIC BANK BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">RHB ISLAMIC BANK BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string" xsi:nil="true"/>
//            <form40Date xsi:type="xsd:dateTime">2011-03-01T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">HALF-YEARLY SINKING FUND OF RM50,000.00 IN THE FORM OF MGIA TO BE HELD ON LIEN TO THE BANK. PROFIT ACCUMULATION FROM THE MGIA SHALL BE RETAINED TO FORM PART OF THE SECURITY
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FIRST PARTY LETTER OF SET-OFF GENERAL INVESTMENT ACCOUNT (S) (GIA) AND MEMORANDUM OF DEPOSIT OF MUDHARABAH GENERAL INVESTMENT ACCOUNT RECEIPT
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">5000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2011-09-12T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">019
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">2ND FLOOR, PODIUM BLOCK, KENANGA INTERNATIONAL
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">JALAN SULTAN ISMAIL
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">50250,KUALA LUMPUR,
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">ASIAN FINANCE BANK BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">ASIAN FINANCE BANK BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2011-09-14T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">CASH DEPOSIT OF RM2,000,000.00 ONLY.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT AND AUTHORITY TO SET OFF (FIRST PARTY)
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">5300000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2012-02-23T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">020
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">LEVEL 11 MENARA YAYASAN TUN RAZAK
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">200 JALAN BUKIT BINTANG
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">RHB ISLAMIC BANK BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">RHB ISLAMIC BANK BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2012-03-29T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">A SUM EQUIVALENT TO 20% OF THE VALUE OF THE MULTI TRADE LINE FACILITIES UTILISED UNDER THE MTL-I FACILITY PLACED IN MGIA, WHICH CAN BE RELEASED UPON FULL SETTLEMENT OF THE CORRESPONDING TR-I/AB-I.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT FOR MUDHARABAH GENERAL INVESTMENT ACCOUNT
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">4370000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2012-06-04T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">021
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">FIRST FLOOR, PODIUM BLOCK
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">MENARA BUMIPUTRA
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">21 JALAN MELAKA, 
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2012-06-13T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">THE PROPERTY HELD UDNER GM 5487 NO. LOT 42974 SEKSYEN 9 BANDAR KAJANG, DISTRICT OF HULU LANGAT, SELANGOR.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2015-07-12T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FIRST LEGAL CHARGE - GM 5487
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">4370000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2012-06-04T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">022
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">FIRST FLOOR, PODIUM BLOCK
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">MENARA BUMIPUTRA
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">21 JALAN MELAKA, 
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2012-06-13T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">THE PROPERTY HELD UNDER HS(D)6132 PT NO. 998 MUKIM OF KAJANG, DISTRICT OF ULU LANGAT, SELANGOR.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2015-07-12T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FIRST LEGAL CHARGE - HS(D)6132
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">4370000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2012-06-04T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">023
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">FIRST FLOOR, PODIUM BLOCK 
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">MENARA BUMIPUTRA
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">21 JALAN MELAKA, 
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2012-06-13T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">THE PROPERTY HELD UNDER HS(D)6133 PT NO. 999 MUKIM OF KAJANG, DISTRICT OF ULU LANGAT, SELANGOR.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2015-07-12T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FIRST LEGAL CHARGE - HS(D)6133
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">4370000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2012-06-04T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">024
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">FIRST FLOOR, PODIUM BLOCK
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">MENARA BUMIPUTRA
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">21 JALAN MELAKA, 
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK MUAMALAT MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2012-06-13T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">THE PROPERTY HELD UNDER GM 5488 NO LOT 42975 SEKSYEN 9 BANDAR KAJANG, DISTRICT OF HULU LANGAT, SELANGOR.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2015-07-12T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FIRST LEGAL CHARGE - GM 5488
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">0.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2012-08-06T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">O
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">025
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">LEVEL 15, MENARA STANDARD CHARTERED
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">30 JALAN SULTAN ISMAIL
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">STANDARD CHARTERED BANK MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">STANDARD CHARTERED BANK MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">-- Please 
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2012-08-09T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">ALL THAT PROPERTY HELD UNDR HAKMILIK STRATA PN 50495/M1-B/4/74, LOT 103 SEKSYEN 36, BANDAR PETALING JAYA, DAERAH PETALING, SELANGOR TOGETHER WITH ONE UNIT OF SHOP OFFICE ERECTED THEREON AT B-10-3A, DATARAN 32, JALAN 19/1, 46300 PETALING JAYA, SELANGOR.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2016-09-04T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FACILITIES AGREEMENT, 1ST PARTY CHARGE - PN 50495/M1-B/4/74
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">0.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2012-08-06T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">O
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">026
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">LEVEL 15 MENARA STANDARD CHARTERED
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">30 JALAN SULTAN ISMAIL
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">STANDARD CHARTERED BANK MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">STANDARD CHARTERED BANK MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">-- Please 
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2012-08-09T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">ALL THAT PROPERTY HELD UNDR HAKMILIK STRATA PN 50495/M1-B/5/84, LOT 103 SEKSYEN 36, BANDAR PETALING JAYA, DAERAH PETALING, SELANGOR TOGETHER WITH ONE UNIT OF SHOP OFFICE ERECTED THEREON AT B-10-05, DATARAN 32, JALAN 19/1, 46300 PETALING JAYA, SELANGOR.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2016-09-04T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">FACILITIES AGREEMENT, 1ST PARTY CHARGE - PN 50495/M1-B/5/84
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">5000000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2013-02-28T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">027
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">U
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">2ND FLOOR, PODIUM BLOCK, KENANGA INTERNATIONAL
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">JALAN SULTAN ISMAIL
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">ASIAN FINANCE BANK BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">ASIAN FINANCE BANK BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2013-03-07T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">THE QUARTELY SINKING FUND OF RM50,000.00 UNTIL IT REACHES RM1,000,000.00 IN THE FORM OF MUDHARABAH GENERAL INVESTMENT ACCOUNT (MGIA).
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT AND AUTHORITY TO SET OFF (FIRST PARTY)
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">6500000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2014-10-06T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">028
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">LEVEL 32, MENARA BANK ISLAM
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">NO. 22, JALAN PERAK
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK ISLAM MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK ISLAM MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2014-10-13T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">(1) ONE (1) UNIT OF TWO STOREY TERRACE HOUSE BEARING ADDRESS NO. 50, JLN PJU 1A/3E, ARA DAMANSARA, PJU 1A, PETALING JAYA SELANGOR HELD UNDER TITLE H.S.(D) 185890, PT31188, MUKIM DAMANSARA, DISTRICT OF PETALING, STATE OF SELANGOR; (2) SINKING FUND OF RM507,674-44 AS AT 3RD MARCH 2014 IS TO BE RETAINED.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2014-11-19T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">(1) 3RD PARTY 1ST LEGAL CHARGE; (2)  MEMORANDUM OF DEPOSIT.
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//          <SSMRegistrationChargesInfos xsi:type="ns2:SSMRegistrationChargesInfo">
//            <ammendNo xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeAmount xsi:type="soapenc:decimal">6500000.00
//            </chargeAmount>
//            <chargeCreateDate xsi:type="xsd:dateTime">2014-10-06T16:00:00.000Z
//            </chargeCreateDate>
//            <chargeCreateDate1 xsi:type="soapenc:string" xsi:nil="true"/>
//            <chargeMortgageType xsi:type="soapenc:string">A
//            </chargeMortgageType>
//            <chargeNo xsi:type="soapenc:string">029
//            </chargeNo>
//            <chargeStatus xsi:type="soapenc:string">S
//            </chargeStatus>
//            <chargeType xsi:type="soapenc:string">C
//            </chargeType>
//            <chargeeAddr1 xsi:type="soapenc:string">LEVEL 32, MENARA BANK ISLAM
//            </chargeeAddr1>
//            <chargeeAddr2 xsi:type="soapenc:string">NO.22, JALAN PERAK
//            </chargeeAddr2>
//            <chargeeAddr3 xsi:type="soapenc:string">
//            </chargeeAddr3>
//            <chargeeId xsi:type="soapenc:string">BANK ISLAM MALAYSIA BERHAD
//            </chargeeId>
//            <chargeeName xsi:type="soapenc:string">BANK ISLAM MALAYSIA BERHAD
//            </chargeeName>
//            <checkDigit xsi:type="soapenc:string">P
//            </checkDigit>
//            <companyName xsi:type="soapenc:string">PC3 TECHNOLOGY SDN. BHD.
//            </companyName>
//            <companyNo xsi:type="soapenc:string">582342
//            </companyNo>
//            <currency xsi:type="soapenc:string">RM
//            </currency>
//            <form40Date xsi:type="xsd:dateTime">2014-10-27T16:00:00.000Z
//            </form40Date>
//            <propertiesAffected xsi:type="soapenc:string">SINKING FUND OF RM507,674.44 AS AT 3RD MARCH 2014 IS TO BE RETAINED.
//            </propertiesAffected>
//            <releaseDate xsi:type="xsd:dateTime">2016-12-10T16:00:00.000Z
//            </releaseDate>
//            <totalOfCharge xsi:type="soapenc:string" xsi:nil="true"/>
//            <typeOfInstrument xsi:type="soapenc:string">MEMORANDUM OF DEPOSIT.
//            </typeOfInstrument>
//          </SSMRegistrationChargesInfos>
//        </SSMRegistrationChargesInfos>
//        <errorMsg xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//        <infoId xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//        <successCode xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">00
//        </successCode>
//      </getInfoChargeListReturn>
//    </ns1:getInfoChargeListResponse>
//  </soapenv:Body>
//</soapenv:Envelope>
//
