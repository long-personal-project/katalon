import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.ras.utilities.WebisComparison

import internal.GlobalVariable

def response = WS.sendRequest(findTestObject(
	'WEBIS/SOAP/getInfoBranchListing',
	[
		brNo: '002316920',
		hostname: GlobalVariable.hostname
	]
))

WebisComparison.verifyResponseValues(response, [
	['getInfoBranchListingResponse.getInfoBranchListingReturn.successCode', '00'],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.errorMsg', ''],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.location', 'KD'],
])

//<?xml version="1.0" encoding="UTF-8"?>
//<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
//  <soapenv:Body>
//	<ns1:getInfoBranchListingResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://servlet.web.webis.ssm.com">
//	  <getInfoBranchListingReturn xsi:type="ns2:SSMRegistrationBranchInfo" xmlns:ns2="urn:SsmWebIs">
//		<checkDigit xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//		<endDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//		<errorMsg xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//		<infoId xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//		<lastUpdateDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//		<location xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">KD
//		</location>
//		<noOfBranches xsi:type="xsd:int">0
//		</noOfBranches>
//		<regName xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//		<regNo xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//		<ssmRegistrationBranchAddressInfo xsi:type="ns2:SSMRegistrationBranchAddressInfo" xsi:nil="true"/>
//		<ssmRegistrationBranchAddressInfos soapenc:arrayType="xsd:anyType[0]" xsi:type="soapenc:Array" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//		<startDate xsi:type="xsd:dateTime">2019-03-04T06:02:06.000Z
//		</startDate>
//		<successCode xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">00
//		</successCode>
//	  </getInfoBranchListingReturn>
//	</ns1:getInfoBranchListingResponse>
//  </soapenv:Body>
//</soapenv:Envelope>

