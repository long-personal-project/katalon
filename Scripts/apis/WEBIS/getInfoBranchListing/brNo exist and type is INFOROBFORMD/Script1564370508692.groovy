import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.ras.utilities.WebisComparison

import internal.GlobalVariable

def response = WS.sendRequest(findTestObject(
	'WEBIS/SOAP/getInfoBranchListing',
	[
		brNo: '002316920',
		type: 'INFOROBFORMD',
		hostname: GlobalVariable.hostname
	]
))

WebisComparison.verifyResponseValues(response, [
	['getInfoBranchListingResponse.getInfoBranchListingReturn.successCode', '00'],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.errorMsg', ''],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.noOfBranches', '2'],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.ssmRegistrationBranchAddressInfos.ssmRegistrationBranchAddressInfos.addressId', '0'],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.ssmRegistrationBranchAddressInfos.ssmRegistrationBranchAddressInfos.checkDigit', 'H'],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.ssmRegistrationBranchAddressInfos.ssmRegistrationBranchAddressInfos.registrationNo', '002316920'],
])

WebisComparison.verifyDateFormatFromPaths(
	response,
	['getInfoBranchListingResponse.getInfoBranchListingReturn.endDate'],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.startDate'],
)

//<?xml version="1.0" encoding="UTF-8"?>
//<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
//  <soapenv:Body>
//	<ns1:getInfoBranchListingResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://servlet.web.webis.ssm.com">
//	  <getInfoBranchListingReturn xsi:type="ns2:SSMRegistrationBranchInfo" xmlns:ns2="urn:SsmWebIs">
//		<checkDigit xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">H
//		</checkDigit>
//		<endDate xsi:type="xsd:dateTime">2021-01-01T16:00:00.000Z
//		</endDate>
//		<errorMsg xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//		<infoId xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">38545428
//		</infoId>
//		<lastUpdateDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//		<location xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">KD
//		</location>
//		<noOfBranches xsi:type="xsd:int">2
//		</noOfBranches>
//		<regName xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">TERAJU BIJAKSANA ENTERPRISE
//		</regName>
//		<regNo xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">002316920
//		</regNo>
//		<ssmRegistrationBranchAddressInfo xsi:type="ns2:SSMRegistrationBranchAddressInfo" xsi:nil="true"/>
//		<ssmRegistrationBranchAddressInfos soapenc:arrayType="xsd:anyType[1]" xsi:type="soapenc:Array" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
//		  <ssmRegistrationBranchAddressInfos xsi:type="ns2:SSMRegistrationBranchAddressInfo">
//			<addressId xsi:type="xsd:long">0
//			</addressId>
//			<bizAddressId xsi:type="soapenc:long" xsi:nil="true"/>
//			<checkDigit xsi:type="soapenc:string">H
//			</checkDigit>
//			<chrstate xsi:type="soapenc:string">B
//			</chrstate>
//			<registrationNo xsi:type="soapenc:string">002316920
//			</registrationNo>
//			<vchaddress xsi:type="soapenc:string">NO. 11, JALAN 6/2,
//			</vchaddress>
//			<vchaddress1 xsi:type="soapenc:string">SEKSYEN 6, BANDAR RINCHING
//			</vchaddress1>
//			<vchaddress2 xsi:type="soapenc:string">
//			</vchaddress2>
//			<vchpostcode xsi:type="soapenc:string">43500
//			</vchpostcode>
//			<vchtown xsi:type="soapenc:string">SEMENYIH
//			</vchtown>
//		  </ssmRegistrationBranchAddressInfos>
//		</ssmRegistrationBranchAddressInfos>
//		<startDate xsi:type="xsd:dateTime">2019-03-04T06:02:06.000Z
//		</startDate>
//		<successCode xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">00
//		</successCode>
//	  </getInfoBranchListingReturn>
//	</ns1:getInfoBranchListingResponse>
//  </soapenv:Body>
//</soapenv:Envelope>

