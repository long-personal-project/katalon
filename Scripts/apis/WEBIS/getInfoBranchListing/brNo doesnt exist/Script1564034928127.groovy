import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.ras.utilities.WebisComparison

import internal.GlobalVariable

def response = WS.sendRequest(findTestObject(
	'WEBIS/SOAP/getInfoBranchListing',
	[
		brNo: 'lorem',
		hostname: GlobalVariable.hostname
	]
))

WebisComparison.verifyResponseValues(response, [
	['getInfoBranchListingResponse.getInfoBranchListingReturn.successCode', '00'],
	['getInfoBranchListingResponse.getInfoBranchListingReturn.errorMsg', 'No Data'],
])

//<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
//<soapenv:Body>
//   <ns1:getInfoBranchListingResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://servlet.web.webis.ssm.com">
//	  <getInfoBranchListingReturn xsi:type="ns2:SupplyBranchResp" xmlns:ns2="urn:SsmWebIs">
//		 <errorMsg xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">No Data</errorMsg>
//		 <infoId xsi:type="soapenc:string" xsi:nil="true" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"/>
//		 <lastUpdateDate xsi:type="xsd:dateTime" xsi:nil="true"/>
//		 <successCode xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">00</successCode>
//	  </getInfoBranchListingReturn>
//   </ns1:getInfoBranchListingResponse>
//</soapenv:Body>
//</soapenv:Envelope>