import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.ras.utilities.WebisComparison as WebisComparison

response = WS.sendRequest(findTestObject(
	'WEBIS/SOAP/isOwnerValidWithIc',
	[
		name: '艾莉森达',
		IcNo: '891123145632',
		agencyId: 'MB',
		hostname: GlobalVariable.hostname
	]
))

WebisComparison.verifyResponseValues(
	response,
	[
		['isOwnerValidWithIcResponse.isOwnerValidWithIcReturn.valid', 'false'],
		['isOwnerValidWithIcResponse.isOwnerValidWithIcReturn.exist', 'true'],
		['isOwnerValidWithIcResponse.isOwnerValidWithIcReturn.errorMsg', 'Name not Match With IC'],
		['isOwnerValidWithIcResponse.isOwnerValidWithIcReturn.icNo', '891123145632'],
		['isOwnerValidWithIcResponse.isOwnerValidWithIcReturn.name', '艾莉森达'],
		['isOwnerValidWithIcResponse.isOwnerValidWithIcReturn.successCode' , '00']
	]
)

