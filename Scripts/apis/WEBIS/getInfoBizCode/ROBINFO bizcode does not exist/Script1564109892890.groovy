import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.ras.utilities.WebisComparison as WebisComparison

response = WS.sendRequest(findTestObject(
	'WEBIS/SOAP/getInfoBizCode',
	[
		tableId: 'ROBINFO',
		businessCode: 'lorem ipsum',
		hostname: GlobalVariable.hostname
	]
))

WebisComparison.verifyResponseValues(
	response,
	[
		['getInfoBizCodeResponse.getInfoBizCodeReturn.businessCode', ''],
		['getInfoBizCodeResponse.getInfoBizCodeReturn.errorMsg', 'No Data'],
		['getInfoBizCodeResponse.getInfoBizCodeReturn.successCode', '00'],
	]
)

//<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
//<soapenv:Body>
//   <ns1:getInfoBizCodeResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://servlet.web.webis.ssm.com">
//	  <getInfoBizCodeReturn xsi:type="ns2:SupplyBizCodeResp" xmlns:ns2="urn:SsmWebIs">
//		 <errorMsg xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">No Data</errorMsg>
//		 <successCode xsi:type="soapenc:string" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">00</successCode>
//	  </getInfoBizCodeReturn>
//   </ns1:getInfoBizCodeResponse>
//</soapenv:Body>
//</soapenv:Envelope>